OBJECTS		 = main.o qr_code.o
OUTPUTS		 = qrgen
DEFS		 = 
OPTIMIZE	 = -Os
LIBS		 = -lgd
LIBDIRS		 = 
INCLUDES	 = -I./
CFLAGS		 = -Wall -pedantic $(INCLUDES) $(OPTIMIZE)
CXXFLAGS	 = $(CFLAGS)
LDFLAGS		 = $(LIBDIRS) $(LIBS) -g
CC		 = gcc
CXX		 = g++
RM		 = rm

all: $(OBJECTS) $(OUTPUTS)
%.o: %.m
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(DEFS) -c -o $@ $<
qrgen: $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJECTS)
clean:
	$(RM) -f $(OUTPUTS) $(OBJECTS)
release: clean

