#ifndef QR_CODE_HPP_
#define QR_CODE_HPP_

#include <stdint.h>

enum QrCodeEccLev {
  QRCODE_ECC_L = 1,
  QRCODE_ECC_M = 0,
  QRCODE_ECC_Q = 3,
  QRCODE_ECC_H = 2
};

struct QrCode {
  QrCode(int ver=6, int ecclev=QRCODE_ECC_M);
  ~QrCode();
  //--- Variables
  int ver;
  int ecclev;
  int mods;
  // this is actually bits...
  int size;
  int ptr;
  uint8_t *data;
  //--- Functions
  bool generateCode(const char *fname);
  bool appendBit(bool val);
  bool appendBits(int val, int bitnum);
  // Each digit in the number needs its own entry
  void appendDataNumeric(int *num, int len);
  void appendDataAlphanum(int *c, int len);
  void appendData8bit(uint8_t *data, int len);
};

#endif

