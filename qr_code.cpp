#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gd.h>

#include "qr_code.hpp"

int versize[41] = { 0,
    26,  44,  70, 100, 134,
   172, 196, 242, 292, 346,
   404, 466, 532, 581, 655,
   733, 815, 901, 991,1085,
  1156,1258,1364,1474,1588,
  1706,1828,1921,2051,2185,
  2323,2465,2611,2761,2876,
  3034,3196,3362,3532,3706,
};

int modsize[41] = { 0,
   21, 25, 29, 33, 37,
   41, 45, 49, 53, 57,
   61, 65, 69, 73, 77,
   81, 85, 89, 93, 97,
  101,105,109,113,117,
  121,125,129,133,137,
  141,145,149,153,157,
  161,165,169,173,177,
};

// Don't want to bother calculating the BCH each time...
uint16_t fmttable[0x20] = {
  0x77C4, 0x72F3, 0x7DAA, 0x789D, 0x662F, 0x6318, 0x6C41, 0x6976,
  0x5412, 0x5125, 0x5E7C, 0x5B4B, 0x45F9, 0x40CE, 0x4F97, 0x4AA0,
  0x355F, 0x3068, 0x3F31, 0x3A06, 0x24B4, 0x2183, 0x2EDA, 0x2BED,
  0x1689, 0x13BE, 0x1CE7, 0x19D0, 0x0762, 0x0255, 0x0D0C, 0x083B,
};

int datasize[41][4] = { {0,0,0,0},
  {  16,  19,   9,  13}, {  28,  34,  16,  22}, {  44,  55,  26,  34}, {  64,  80,  36,  48},
  {  86, 108,  46,  62}, { 108, 136,  60,  76}, { 124, 156,  66,  88}, { 154, 194,  86, 110},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
  {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0}, {   0,   0,   0,   0},
};

QrCode::QrCode(int _ver, int _ecclev) : ver(_ver), ecclev(_ecclev)
{
  size = datasize[ver][ecclev]*8;
  mods = modsize[ver];
  data = (uint8_t*)malloc(size);
  memset(data, 0, size);
  ptr = 0;
}

QrCode::~QrCode()
{
  free(data);
}

//http://raidenii.net/sc/ket/0000111122223333/IKD
//47

#define DRAW_BIG_TRACK(x, y, adjx, adjy) \
  gdImageRectangle(im, x, y, x+7, y+7, white); \
  gdImageFilledRectangle(im, x+1-adjx, y+1-adjy, x+7-adjx, y+7-adjy, black); \
  gdImageRectangle(im, x+2-adjx, y+2-adjy, x+6-adjx, y+6-adjy, white); \
  for(int k=0; k < 8+adjx; k++) for(int j=0; j < 8; j++) codeUsed[x+k + ((y+j)*mods)] = 1;

#define DRAW_TRACK(x, y) \
  gdImageFilledRectangle(im, x, y, x+4, y+4, black); \
  gdImageRectangle(im, x+1, y+1, x+3, y+3, white); \
  for(int k=0; k < 5; k++) for(int j=0; j < 5; j++) codeUsed[x+k + ((y+j)*mods)] = 1;

bool QrCode::generateCode(const char *fname)
{
  uint8_t *codeUsed;
  uint8_t *finalData;
  int styleDotted[2];
  gdImagePtr im;
  int black, white;
  int bound = mods-1;
  codeUsed = (uint8_t*)malloc(mods*mods);
  memset(codeUsed, 0, mods*mods);
  finalData = (uint8_t*)malloc(versize[ver]*8);
  im = gdImageCreate(mods, mods);
  black = gdImageColorAllocate(im, 0,0,0);
  white = gdImageColorAllocate(im, 255,255,255);
  styleDotted[0] = black;
  styleDotted[1] = white;
  gdImageSetStyle(im, styleDotted, 2);
  gdImageFilledRectangle(im, 0, 0, bound, bound, white);

  // Draw basic tracking
  DRAW_BIG_TRACK(0,0, 1,1);
  DRAW_BIG_TRACK(bound-7,0, 0,1);
  DRAW_BIG_TRACK(0,bound-7, 1,0);
  // Draw timing
  gdImageLine(im, 8, 6, bound-7, 6, gdStyled);
  gdImageLine(im, 6, 8, 6, bound-7, gdStyled);
  for(int k=0; k <= bound-16; k++) codeUsed[8+k + (6*mods)] = 1;
  for(int k=0; k <= bound-16; k++) codeUsed[6 + ((8+k)*mods)] = 1;

  // Draw additional tracking
  if(ver > 1) {
    DRAW_TRACK(bound-8, bound-8);
  }
  if(ver > 6) {
    int bmod = (ver - 7) * 2;
    DRAW_TRACK(bound-24 - bmod, bound-8);
    DRAW_TRACK(bound-8, bound-24 - bmod);
    DRAW_TRACK(bound-24 - bmod, bound-24 - bmod);
    DRAW_TRACK(bound-24 - bmod, 4);
    DRAW_TRACK(4, bound-24 - bmod);
  }

  // Finish up the user data
  { int i;
    for(i=0; i < ptr; i++) {
      finalData[i] = data[i];
    }
    for(; i < (ptr + (7-(ptr%8))); i++) {
      finalData[i] = 0;
    }
    for(int k=0; i < size; i += 8, k ^= 1) {
      finalData[i+0] = 1 ^ k;
      finalData[i+1] = 1 ^ k;
      finalData[i+2] = 1 ^ k;
      finalData[i+3] = 0 ^ k;
      finalData[i+4] = 1 ^ k;
      finalData[i+5] = 1 ^ k;
      finalData[i+6] = 1 ^ k;
      finalData[i+7] = 0 ^ k;
    }
    // Build the ECC
    for(int s = i; s < versize[ver]*8; s++) // Clear the ECC area
      finalData[s] = 0;
  }

  // Choose mask pattern (force to 6)
  int mask_ptrn = 6;

  // Draw data
  int x, y;
  x = y = bound;
  int dir = 0, xfsm = 0;
  for(int i=0; i < versize[ver]*8; i++) {
    int mask;
    mask = (((x * y) % 2) + ((x * y) % 3)) % 2 == 0;
    if(codeUsed[x + (y*mods)]) // If we've got a tracking block, skip it
      i--;
    else
      gdImageSetPixel(im, x, y, (finalData[i] ^ mask) ? black : white);
    if(dir == 0) {
      if(xfsm == 0)
        x--;
      else {
        x++;
        y--;
      }
    }else{
      if(xfsm == 0)
        x--;
      else {
        x++;
        y++;
      }
    }
    xfsm ^= 1;
    if(dir == 0) {
      if((x > bound-8) || (x <= 8)) {
        if(y <= 8) {
          dir = 1;
          x -= 2;
          if(x == 6) x--;
          if((x >= bound-8) || (x <= 8))
            y = 9;
          else
            y = 0;
        }
      }else{
        if(y < 0) {
          dir = 1;
          x -= 2;
          if(x == 6) x--;
          if((x >= bound-8) || (x <= 8))
            y = 9;
          else
            y = 0;
        }
      }
    }else{
      if(x < 8) {
        if(y >= bound-8) {
          dir = 0;
          x -= 2;
          if(x == 6) x--;
        }
      }else{
        if(y > bound) {
          dir = 0;
          y--;
          x -= 2;
          if(x == 6) x--;
        }
      }
    }
  }

  // Draw format information
  uint16_t fmtinfo = fmttable[(ecclev << 3) | mask_ptrn];
  gdImageSetPixel(im, 8, bound-7, black);
  if(fmtinfo & 0x0001) { gdImageSetPixel(im, 8, 0, black); gdImageSetPixel(im, bound-0, 8, black); }
  if(fmtinfo & 0x0002) { gdImageSetPixel(im, 8, 1, black); gdImageSetPixel(im, bound-1, 8, black); }
  if(fmtinfo & 0x0004) { gdImageSetPixel(im, 8, 2, black); gdImageSetPixel(im, bound-2, 8, black); }
  if(fmtinfo & 0x0008) { gdImageSetPixel(im, 8, 3, black); gdImageSetPixel(im, bound-3, 8, black); }
  if(fmtinfo & 0x0010) { gdImageSetPixel(im, 8, 4, black); gdImageSetPixel(im, bound-4, 8, black); }
  if(fmtinfo & 0x0020) { gdImageSetPixel(im, 8, 5, black); gdImageSetPixel(im, bound-5, 8, black); }
  if(fmtinfo & 0x0040) { gdImageSetPixel(im, 8, 7, black); gdImageSetPixel(im, bound-6, 8, black); }
  if(fmtinfo & 0x0080) { gdImageSetPixel(im, 8, 8, black); gdImageSetPixel(im, bound-7, 8, black); }
  if(fmtinfo & 0x0100) { gdImageSetPixel(im, 7, 8, black); gdImageSetPixel(im, 8, bound-6, black); }
  if(fmtinfo & 0x0200) { gdImageSetPixel(im, 5, 8, black); gdImageSetPixel(im, 8, bound-5, black); }
  if(fmtinfo & 0x0400) { gdImageSetPixel(im, 4, 8, black); gdImageSetPixel(im, 8, bound-4, black); }
  if(fmtinfo & 0x0800) { gdImageSetPixel(im, 3, 8, black); gdImageSetPixel(im, 8, bound-3, black); }
  if(fmtinfo & 0x1000) { gdImageSetPixel(im, 2, 8, black); gdImageSetPixel(im, 8, bound-2, black); }
  if(fmtinfo & 0x2000) { gdImageSetPixel(im, 1, 8, black); gdImageSetPixel(im, 8, bound-1, black); }
  if(fmtinfo & 0x4000) { gdImageSetPixel(im, 0, 8, black); gdImageSetPixel(im, 8, bound-0, black); }

  

  FILE *fp = fopen(fname, "wb");
  gdImagePng(im, fp);
  fclose(fp);
  gdImageDestroy(im);
  free(codeUsed);
  free(finalData);
  return true;
}

bool QrCode::appendBit(bool val)
{
  if(ptr >= size)
    return false;
  data[ptr++] = val;
  return true;
}

bool QrCode::appendBits(int val, int bitnum)
{
  for(int i=bitnum-1; i >= 0; i--) {
    if(!appendBit((val >> i) & 1))
      return false;
  }
  return true;
}

void QrCode::appendDataNumeric(int *_num, int len)
{
  // Mode indicator
  appendBits(1, 4);
  // Append character count
  appendBits(len, 10);
  for(int i=0; i < len;) {
    int num = 0;
    int z;
    for(z=0; z < 3 && i < len; z++, i++) {
      num *= 10;
      num += _num[i];
    }
    // Append characters
    if(z == 3)
      appendBits(num, 10);
    else if(z == 2)
      appendBits(num, 7);
    else if(z == 1)
      appendBits(num, 4);
  }
}

void QrCode::appendDataAlphanum(int *c, int len)
{
  for(int i=0; i < len; i++) {
  }
}

void QrCode::appendData8bit(uint8_t *data, int len)
{
  for(int i=0; i < len; i++) {
  }
}


