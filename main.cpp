#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "qr_code.hpp"

int main(int argc, char **argv)
{
  QrCode code(5, QRCODE_ECC_L);

  for(int i=0; i < 64; i++)
    code.appendBits(0xAA, 8);

  code.generateCode(argv[1]);

  return EXIT_SUCCESS;
}

